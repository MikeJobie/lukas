@extends('layouts.app')

@section('content')

        <div class="container py-3">
            <div class="row">
                        <div class="col-md-8 offset-md-2">

                            <div class="card">
                                    <div class="card-header">
                                    <h1>Edit Post</h1>
                                    </div>

                                  <div class="card-body">
                                    <form method="POST" action="{{ route('post.update', $post->id) }}">
                                        @csrf
                                        @method('PUT')

                                        <div class="form-group">
                                            <label for="title">Title</label>
                                            <input type="text" name="title" class="form-control" value="{{ $post->title }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="description">Description</label>
                                            <textarea name="description" rows="8" cols="80" class="form-control">{{ $post->description }}</textarea>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Update Post</button>
                                    </form>
                                 </div>

                            </div>
                        </div>
                </div>
        </div>

    @endsection